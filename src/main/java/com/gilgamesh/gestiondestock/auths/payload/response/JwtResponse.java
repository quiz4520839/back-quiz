package com.gilgamesh.gestiondestock.auths.payload.response;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.List;

@Getter
@Setter
public class JwtResponse {
  private String token;
  private String type = "Bearer";
  private Long id;
  private String username;
  private String email;
  private List<String> roles;

  private String nom;

  private String prenom;

  private String num;

  private Character sexe;

  public JwtResponse(
          String accessToken,
          Long id,
          String nom,
          String prenom,
          String num,
          Character sexe,
          String username,
          String email,
          List<String> roles)
  {
    this.token = accessToken;
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.num = num;
    this.sexe = sexe;
    this.username = username;
    this.email = email;
    this.roles = roles;
  }

}
