package com.gilgamesh.gestiondestock.auths.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

import javax.persistence.Column;

@Setter
@Getter
public class SignupRequest {

  @Column(name = "lastname")
  private String nom;

  @Column(name = "firstname")
  private String prenom;

  @Column(name = "phone")
  private String num;

  @Column(name = "sexe")
  private Character sexe;

  @Column(nullable = false)
  private String username;

  @Column(nullable = false, length = 50)
  private String email;

  private Set<String> role;

  @Column(nullable = false, length = 40)
  private String password;

}
