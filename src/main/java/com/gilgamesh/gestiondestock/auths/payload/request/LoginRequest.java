package com.gilgamesh.gestiondestock.auths.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Setter
@Getter
public class LoginRequest {
	@Column(nullable = false)
  	private String username;

  	@Column(nullable = false)
	private String password;

}
