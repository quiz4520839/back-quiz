package com.gilgamesh.gestiondestock.auths.security;

import org.springframework.security.config.annotation.web.configurers.CorsConfigurer;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.Filter;

public class CustomCorsFilter extends CorsFilter {

    public CustomCorsFilter() {
        super(configSource());
    }

    private static CorsConfigurationSource configSource() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);

        corsConfiguration.addAllowedHeader("*");

        corsConfiguration.addAllowedOriginPattern("*");

        corsConfiguration.addAllowedMethod("GET");

        corsConfiguration.addAllowedMethod("PUT");

        corsConfiguration.addAllowedMethod("POST");

        corsConfiguration.addAllowedMethod("PATCH");

        corsConfiguration.addAllowedMethod("DELETE");

        corsConfiguration.addAllowedMethod("OPTIONS");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }
}
