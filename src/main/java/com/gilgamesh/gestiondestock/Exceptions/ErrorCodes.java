package com.gilgamesh.gestiondestock.Exceptions;

public enum ErrorCodes {
    UTILISATEUR_NOT_FOUND(1000),
    UTILISATEUR_NOT_VALID(1001),

    QUESTION_NOT_FOUND(2000),
    QUESTION_NOT_VALID(2001),

    QUESTIONAIRE_NOT_FOUND(3000),
    QUESTIONAIRE_NOT_VALID(3001),

    EVALUATION_NOT_FOUND(4000),
    EVALUATION_NOT_VALID(4001)
    ;

    private int code;
    ErrorCodes(int code){this.code = code;}

    public int getCode(){return code;}
}
