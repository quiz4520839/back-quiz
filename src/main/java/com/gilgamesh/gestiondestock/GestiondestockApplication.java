package com.gilgamesh.gestiondestock;


import com.gilgamesh.gestiondestock.models.ERole;
import com.gilgamesh.gestiondestock.models.Roles;
import com.gilgamesh.gestiondestock.models.Utilisateur;
import com.gilgamesh.gestiondestock.repository.RolesRepository;
import com.gilgamesh.gestiondestock.repository.UtilisateurRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
@EnableJpaAuditing
public class GestiondestockApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestiondestockApplication.class, args);
	}

//	@Bean
//	public CorsFilter corsFilter(){
//
//		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//
//		final CorsConfiguration corsConfiguration = new CorsConfiguration();
//
//		corsConfiguration.setAllowCredentials(true);
//
//		corsConfiguration.addAllowedHeader("*");
//
//		corsConfiguration.addAllowedOriginPattern("*");
//
//		corsConfiguration.addAllowedMethod("GET");
//
//		corsConfiguration.addAllowedMethod("PUT");
//
//		corsConfiguration.addAllowedMethod("POST");
//
//		corsConfiguration.addAllowedMethod("PATCH");
//
//		corsConfiguration.addAllowedMethod("DELETE");
//
//		corsConfiguration.addAllowedMethod("OPTIONS");
//
//		source.registerCorsConfiguration("/**", corsConfiguration);
//
//		return new CorsFilter(source);
//	}

	@Bean
	public ApplicationRunner run(
			AuthenticationManager authenticationManager,
			PasswordEncoder encoder,
			RolesRepository roleRepository,
			UtilisateurRepository userRepository) throws Exception {


		return (ApplicationArguments args) -> {

			if (!userRepository.existsByUsername("admin")){
				// Create new user's account
				Utilisateur user = new Utilisateur("admin", "admin", "admin", "697203525", 'H', "admin@gmail.com",
						encoder.encode("12345"));
				Set<Roles> roles = new HashSet<>();

				roles.add(roleRepository.findByName(ERole.ROLE_ADMIN).get());
				roles.add(roleRepository.findByName(ERole.ROLE_USER).get());
				roles.add(roleRepository.findByName(ERole.ROLE_MODERATOR).get());
				user.setRoles(roles);
				userRepository.save(user);
			}

		};

	}

}
