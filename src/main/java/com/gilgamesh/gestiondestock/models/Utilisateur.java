package com.gilgamesh.gestiondestock.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class Utilisateur extends AbstractEntity{

    @Column(name = "lastname")
    private String nom;

    @Column(name = "firstname")
    private String prenom;

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "phone")
    private String num;

    @Column(name = "sexe")
    private Character sexe;

    @Column(name = "picture")
    private String photo;

    @OneToMany(mappedBy = "utilisateur")
    private List<Evaluation> evaluationList;

    @OneToMany(mappedBy = "utilisateur")
    private List<Questionaire> questionaires;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(  name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Roles> roles = new HashSet<>();

    public Utilisateur(String username, String nom, String prenom, String num, Character sexe, String email, String password) {
        this.username = username;
        this.nom = nom;
        this.prenom = prenom;
        this.num = num;
        this.sexe = sexe;
        this.email = email;
        this.password = password;
    }
}
