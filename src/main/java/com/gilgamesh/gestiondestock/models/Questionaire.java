package com.gilgamesh.gestiondestock.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "quiz")
public class Questionaire extends AbstractEntity{

    @Column(name = "code")
    private String code;

    @Column(name = "title")
    private String titre;

    @Column(name = "description")
    private String description;

    @Column(name = "picture")
    private String image;

    @ManyToOne
    @JoinColumn(name = "idutilisateur")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Utilisateur utilisateur;

    @OneToMany(mappedBy = "questionaire")
    private List<Question> questions;

    @OneToMany(mappedBy = "questionaire")
    private List<Evaluation> evaluations;
}
