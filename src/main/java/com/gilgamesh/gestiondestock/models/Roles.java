package com.gilgamesh.gestiondestock.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "roles")
public class Roles extends AbstractEntity{

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;
}
