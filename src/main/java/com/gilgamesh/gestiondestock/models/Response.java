package com.gilgamesh.gestiondestock.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Response {

    private Long id;

    private String libelle;

    private Boolean exactly;
}
