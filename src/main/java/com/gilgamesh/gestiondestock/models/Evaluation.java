package com.gilgamesh.gestiondestock.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "evaluations",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "identify")
        })
public class Evaluation extends AbstractEntity{

    @Column(name = "evaluationdate")
    private Instant dateEval;

    @Column(name = "score")
    private Integer score;

    @Column(name = "rang")
    private Integer rang;

    @Column(name = "taux")
    private BigDecimal taux;

    @Column(name = "identify", nullable = false)
    private String identify;

    @ManyToOne
    @JoinColumn(name = "idutilisateur")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name = "idquestionaire")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Questionaire questionaire;
}
