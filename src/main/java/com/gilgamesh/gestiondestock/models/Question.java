package com.gilgamesh.gestiondestock.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "question")
public class Question extends AbstractEntity{

    @Column(name = "libelle")
    private String libelle;

    @Column(columnDefinition = "json")
    @Convert(converter = jsonConverter.class)
    private List<Response> responses;

    @ManyToOne
    @JoinColumn(name = "idquestionaire")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Questionaire questionaire;
}
