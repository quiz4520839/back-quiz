package com.gilgamesh.gestiondestock.services.impls;

import com.gilgamesh.gestiondestock.models.Utilisateur;
import com.gilgamesh.gestiondestock.repository.UtilisateurRepository;
import com.gilgamesh.gestiondestock.services.service.IUtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UtilisateurServiceImpl implements IUtilisateurService {

    private UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    @Override
    public Utilisateur saveUser(Utilisateur utilisateur) {
        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public List<Utilisateur> allUsers() {
        List<Utilisateur> utilisateurs = new ArrayList<>();
        utilisateurRepository.findAll().forEach(utilisateur -> {
            utilisateurs.add(utilisateur);
        });
        return utilisateurs;
    }

    @Override
    public Optional<Utilisateur> findUserById(Long id) {
        return utilisateurRepository.findById(id);
    }

    @Override
    public Utilisateur deleteUserById(Long id) {
        return null;
    }
}
