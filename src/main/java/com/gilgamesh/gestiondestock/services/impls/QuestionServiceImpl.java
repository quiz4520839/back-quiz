package com.gilgamesh.gestiondestock.services.impls;

import com.gilgamesh.gestiondestock.models.Question;
import com.gilgamesh.gestiondestock.repository.QuestionRepository;
import com.gilgamesh.gestiondestock.services.service.IQuestionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class QuestionServiceImpl implements IQuestionService {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question saveQuestion(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public List<Question> allQuestion() {
        List<Question> questions = new ArrayList<>();
        questionRepository.findAll().forEach(question -> {
            questions.add(question);
        });
        return questions;
    }

    @Override
    public Question findQuestionById(Long id) {
        return questionRepository.findById(id).get();
    }

    @Override
    public void deleteQuestionById(Long id) {
        questionRepository.deleteById(id);
    }
}
