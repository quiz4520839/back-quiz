package com.gilgamesh.gestiondestock.services.impls;

import com.gilgamesh.gestiondestock.models.PageSettings;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.repository.QuestionaireRepository;
import com.gilgamesh.gestiondestock.services.service.IQuestionaireService;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional
public class QuestionaireServiceImpl implements IQuestionaireService {

    private QuestionaireRepository questionaireRepository;

    @Autowired
    public QuestionaireServiceImpl(QuestionaireRepository questionaireRepository) {
        this.questionaireRepository = questionaireRepository;
    }

    @Override
    public Questionaire saveQuestionaire(Questionaire questionaire) {
        return questionaireRepository.save(questionaire);
    }

    @Override
    public Page<Questionaire> allQuestionaires(@NotNull PageSettings pageSetting) {
        Sort questionaireSort = pageSetting.builderSort();
        Pageable questionairePage = PageRequest.of(pageSetting.getPage(), pageSetting.getElementPerPage(), questionaireSort);
        return questionaireRepository.findAll(questionairePage);
    }

    @Override
    public Questionaire findQuestionaireById(Long id) {
        return questionaireRepository.findById(id).get();
    }

    @Override
    public void deleteQuestionaireById(Long id) {
        questionaireRepository.deleteById(id);
    }
}
