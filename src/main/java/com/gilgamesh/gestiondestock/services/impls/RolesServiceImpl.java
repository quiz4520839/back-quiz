package com.gilgamesh.gestiondestock.services.impls;

import com.gilgamesh.gestiondestock.models.Roles;
import com.gilgamesh.gestiondestock.repository.RolesRepository;
import com.gilgamesh.gestiondestock.services.service.IRolesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class RolesServiceImpl implements IRolesService {

    private RolesRepository rolesRepository;

    @Autowired
    public RolesServiceImpl(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    @Override
    public Roles saveRole(Roles roles) {
        return rolesRepository.save(roles);
    }

    @Override
    public List<Roles> allRoles() {
        List<Roles> roles = new ArrayList<>();
        rolesRepository.findAll().forEach(role -> {
            roles.add(role);
        });
        return roles;
    }
}
