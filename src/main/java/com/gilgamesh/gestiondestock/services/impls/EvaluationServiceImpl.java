package com.gilgamesh.gestiondestock.services.impls;

import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.repository.EvaluationRepository;
import com.gilgamesh.gestiondestock.services.service.IEvaluationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class EvaluationServiceImpl implements IEvaluationService {

    private EvaluationRepository evaluationRepository;

    @Autowired
    public EvaluationServiceImpl(EvaluationRepository evaluationRepository) {
        this.evaluationRepository = evaluationRepository;
    }

    @Override
    public Evaluation saveEvaluation(Evaluation evaluation) {
        return evaluationRepository.save(evaluation);
    }

    @Override
    public List<Evaluation> allEvaluations() {
        List<Evaluation> evaluations = new ArrayList<>();
        evaluationRepository.findAll().forEach(evaluation -> {
            evaluations.add(evaluation);
        });
        return evaluations;
    }

    @Override
    public List<Evaluation> findEvaluationByQuestionaire(Questionaire questionaire) {
        List<Evaluation> evaluations = new ArrayList<>();
        evaluationRepository.findEvaluationByQuestionaire(questionaire).forEach(evaluation -> {
            evaluations.add(evaluation);
        });
        return evaluations;
    }

    @Override
    public Evaluation findEvaluationById(Long id) {
        return evaluationRepository.findById(id).get();
    }

    @Override
    public void deleteEvaluationById(Long id) {
        evaluationRepository.deleteById(id);
    }
}
