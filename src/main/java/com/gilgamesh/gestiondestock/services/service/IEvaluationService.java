package com.gilgamesh.gestiondestock.services.service;

import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;

import java.util.List;

public interface IEvaluationService {

    public Evaluation saveEvaluation(Evaluation evaluation);

    public List<Evaluation> allEvaluations();

    List<Evaluation> findEvaluationByQuestionaire(Questionaire questionaire);

    public Evaluation findEvaluationById(Long id);

    public void deleteEvaluationById(Long id);
}
