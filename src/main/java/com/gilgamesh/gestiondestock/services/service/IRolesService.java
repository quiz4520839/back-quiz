package com.gilgamesh.gestiondestock.services.service;

import com.gilgamesh.gestiondestock.models.Roles;
import com.gilgamesh.gestiondestock.models.Utilisateur;

import java.util.List;

public interface IRolesService {

    public Roles saveRole(Roles roles);

    public List<Roles> allRoles();
}
