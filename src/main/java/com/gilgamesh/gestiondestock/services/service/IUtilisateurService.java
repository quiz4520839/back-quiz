package com.gilgamesh.gestiondestock.services.service;

import com.gilgamesh.gestiondestock.models.Utilisateur;

import java.util.List;
import java.util.Optional;

public interface IUtilisateurService {

    public Utilisateur saveUser(Utilisateur utilisateur);

    public List<Utilisateur> allUsers();

    public Optional<Utilisateur> findUserById(Long id);

    public Utilisateur deleteUserById(Long id);
}
