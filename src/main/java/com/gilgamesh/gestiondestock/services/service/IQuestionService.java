package com.gilgamesh.gestiondestock.services.service;

import com.gilgamesh.gestiondestock.models.Question;

import java.util.List;

public interface IQuestionService {

    public Question saveQuestion(Question question);

    public List<Question> allQuestion();

    public Question findQuestionById(Long id);

    public void deleteQuestionById(Long id);
}
