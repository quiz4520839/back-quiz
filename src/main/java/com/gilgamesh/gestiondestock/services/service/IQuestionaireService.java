package com.gilgamesh.gestiondestock.services.service;

import com.gilgamesh.gestiondestock.models.PageSettings;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.models.Utilisateur;
import com.sun.istack.NotNull;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IQuestionaireService {

    public Questionaire saveQuestionaire(Questionaire questionaire);

    public Page<Questionaire> allQuestionaires(@NotNull PageSettings pageSetting);

    public Questionaire findQuestionaireById(Long id);

    public void deleteQuestionaireById(Long id);
}
