package com.gilgamesh.gestiondestock.web.api;

import com.gilgamesh.gestiondestock.models.PageDTO;
import com.gilgamesh.gestiondestock.models.PageSettings;
import com.gilgamesh.gestiondestock.models.PageToPageDTOMapper;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.services.service.IQuestionaireService;
import com.gilgamesh.gestiondestock.web.controllers.QuestionaireController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuestionaireApi implements QuestionaireController {

    private IQuestionaireService questionaireService;

    private PageToPageDTOMapper<Questionaire> pageToPageDTOMapper;

    @Autowired
    public QuestionaireApi(IQuestionaireService questionaireService, PageToPageDTOMapper<Questionaire> pageToPageDTOMapper) {
        this.questionaireService = questionaireService;
        this.pageToPageDTOMapper = pageToPageDTOMapper;
    }


    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Questionaire saveQuestionaire(Questionaire questionaire) {
        return questionaireService.saveQuestionaire(questionaire);
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public PageDTO<Questionaire> allQuestionaires(PageSettings pageSettings) {
        //log.info("Request for questionaire received with data : " + pageSettings);
        return pageToPageDTOMapper.pageToPageDTO(questionaireService.allQuestionaires(pageSettings));
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public Questionaire findQuestionaireById(Long id) {
        return questionaireService.findQuestionaireById(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteQuestionaireById(Long id) {
        questionaireService.deleteQuestionaireById(id);
    }
}
