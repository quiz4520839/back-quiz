package com.gilgamesh.gestiondestock.web.api;

import com.gilgamesh.gestiondestock.models.Utilisateur;
import com.gilgamesh.gestiondestock.services.service.IUtilisateurService;
import com.gilgamesh.gestiondestock.web.controllers.UtilisateurController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class UtilisateurApi implements UtilisateurController {

    private IUtilisateurService utilisateurService;

    @Autowired
    public UtilisateurApi(IUtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public List<Utilisateur> allUsers() {
        return utilisateurService.allUsers();
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public Optional<Utilisateur> findUserById(Long id) {
        return utilisateurService.findUserById(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUserById(Long id) {
        utilisateurService.deleteUserById(id);
    }
}
