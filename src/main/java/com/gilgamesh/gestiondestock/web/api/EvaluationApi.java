package com.gilgamesh.gestiondestock.web.api;

import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.services.service.IEvaluationService;
import com.gilgamesh.gestiondestock.web.controllers.EvaluationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EvaluationApi implements EvaluationController {

    private IEvaluationService evaluationService;

    @Autowired
    public EvaluationApi(IEvaluationService evaluationService) {
        this.evaluationService = evaluationService;
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public Evaluation saveEvaluation(Evaluation evaluation) {
        return evaluationService.saveEvaluation(evaluation);
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public List<Evaluation> allEvaluations() {
        return evaluationService.allEvaluations();
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public List<Evaluation> findEvaluationByQuestionaire(Questionaire questionaire) {
        return (List<Evaluation>) evaluationService.findEvaluationByQuestionaire(questionaire);
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public Evaluation findEvaluationById(Long id) {
        return evaluationService.findEvaluationById(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public void deleteEvaluationById(Long id) {
        evaluationService.deleteEvaluationById(id);
    }
}
