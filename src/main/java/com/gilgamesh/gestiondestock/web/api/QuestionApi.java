package com.gilgamesh.gestiondestock.web.api;

import com.gilgamesh.gestiondestock.models.Question;
import com.gilgamesh.gestiondestock.services.service.IQuestionService;
import com.gilgamesh.gestiondestock.web.controllers.QuestionController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QuestionApi implements QuestionController {

    private IQuestionService questionService;

    @Autowired
    public QuestionApi(IQuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Question saveQuestion(Question question) {
        return questionService.saveQuestion(question);
    }

    @Override
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public List<Question> allQuestion() {
        return questionService.allQuestion();
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public Question findQuestionById(Long id) {
        return questionService.findQuestionById(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteQuestionById(Long id) {
        questionService.deleteQuestionById(id);
    }
}
