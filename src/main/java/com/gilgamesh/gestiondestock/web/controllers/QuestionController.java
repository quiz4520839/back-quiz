package com.gilgamesh.gestiondestock.web.controllers;

import com.gilgamesh.gestiondestock.models.Question;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.gilgamesh.gestiondestock.utils.Constants.*;

@RequestMapping(name = QUESTION)
public interface QuestionController {

    @PostMapping(value = CREATE_QUESTION)
    public Question saveQuestion(@RequestBody Question question);

    @GetMapping(value = ALL_QUESTION)
    public List<Question> allQuestion();

    @GetMapping(value = ONE_QUESTION)
    public Question findQuestionById(@PathVariable Long id);

    @DeleteMapping(value = DELETE_QUESTION)
    public void deleteQuestionById(@PathVariable Long id);
}
