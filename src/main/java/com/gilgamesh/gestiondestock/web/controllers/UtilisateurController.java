package com.gilgamesh.gestiondestock.web.controllers;

import com.gilgamesh.gestiondestock.models.Utilisateur;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.gilgamesh.gestiondestock.utils.Constants.*;

@RequestMapping(name = AUTHS)
public interface UtilisateurController {

    @GetMapping(value = GET_ALL_USERS)
    public List<Utilisateur> allUsers();

    @GetMapping(value = GET_USER)
    public Optional<Utilisateur> findUserById(@PathVariable Long id);

    @DeleteMapping(value = DELETE_USER)
    public void deleteUserById(@PathVariable Long id);
}
