package com.gilgamesh.gestiondestock.web.controllers;

import com.gilgamesh.gestiondestock.models.PageDTO;
import com.gilgamesh.gestiondestock.models.PageSettings;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.sun.istack.NotNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.gilgamesh.gestiondestock.utils.Constants.*;

@RequestMapping(name = QUIZ)
public interface QuestionaireController {

    @PostMapping(value = CREATE_QUIZ)
    public Questionaire saveQuestionaire(@RequestBody Questionaire questionaire);

    @GetMapping(value = ALL_QUIZ)
    public PageDTO<Questionaire> allQuestionaires(PageSettings pageSetting);

    @GetMapping(value = ONE_QUIZ)
    public Questionaire findQuestionaireById(@PathVariable Long id);

    @DeleteMapping(value = DELETE_QUIZ)
    public void deleteQuestionaireById(@PathVariable Long id);
}
