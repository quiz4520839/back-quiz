package com.gilgamesh.gestiondestock.web.controllers;

import com.gilgamesh.gestiondestock.auths.payload.request.LoginRequest;
import com.gilgamesh.gestiondestock.auths.payload.request.SignupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.gilgamesh.gestiondestock.utils.Constants.*;

@RequestMapping(name = AUTHS)
public interface AuthController {

    @PostMapping(value = LOGIN)
    public ResponseEntity<?> login( @RequestBody LoginRequest loginRequest);

    @PostMapping(value = REGISTER)
    public ResponseEntity<?> register(@RequestBody SignupRequest signUpRequest);
}
