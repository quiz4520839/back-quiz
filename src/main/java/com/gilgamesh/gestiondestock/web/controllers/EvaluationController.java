package com.gilgamesh.gestiondestock.web.controllers;

import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.gilgamesh.gestiondestock.utils.Constants.*;

@RequestMapping(name = PARTICIPATION)
public interface EvaluationController {

    @PostMapping(value = CREATE_PARTICIPATION)
    public Evaluation saveEvaluation(@RequestBody Evaluation evaluation);

    @GetMapping(value = ALL_PARTICIPATION)
    public List<Evaluation> allEvaluations();

    @PostMapping(value = QUIZ_PARTICIPATION)
    List<Evaluation> findEvaluationByQuestionaire(@RequestBody Questionaire questionaire);

    @GetMapping(value = ONE_PARTICIPATION)
    public Evaluation findEvaluationById(@PathVariable Long id);

    @DeleteMapping(value = DELETE_PARTICIPATION)
    public void deleteEvaluationById(@PathVariable Long id);
}
