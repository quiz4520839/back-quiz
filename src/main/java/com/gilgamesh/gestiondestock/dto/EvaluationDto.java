package com.gilgamesh.gestiondestock.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.models.Utilisateur;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class EvaluationDto {

    private Long id;

    private Instant dateEval;

    private Integer score;

    private Integer rang;

    @JsonIgnore
    private Utilisateur utilisateur;

    @JsonIgnore
    private Questionaire questionaire;

    public EvaluationDto fromEntity(Evaluation evaluation){
        if (evaluation == null){
            return null;
        }
        return EvaluationDto.builder()
                .id(evaluation.getId())
                .dateEval(evaluation.getDateEval())
                .score(evaluation.getScore())
                .rang(evaluation.getRang())
                .build();
    }

    public Evaluation toEntity(EvaluationDto evaluationDto){
        if (evaluationDto == null){
            return null;
        }
        Evaluation evaluation = new Evaluation();

        evaluation.setId(evaluationDto.getId());
        evaluation.setDateEval(evaluationDto.getDateEval());
        evaluation.setScore(evaluationDto.getScore());
        evaluation.setRang(evaluationDto.getRang());

        return evaluation;
    }
}
