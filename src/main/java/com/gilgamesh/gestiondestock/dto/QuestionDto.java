package com.gilgamesh.gestiondestock.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gilgamesh.gestiondestock.models.Question;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.models.Response;
import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@Builder
public class QuestionDto {

    private Long id;

    private String libelle;

    private Response reponses;

    @JsonIgnore
    private Questionaire questionaire;

    public QuestionDto fromEntity(Question question){
        if (question == null){
            return null;
        }
        return QuestionDto.builder()
                .id(question.getId())
                .libelle(question.getLibelle())
                .reponses((Response) question.getResponses())
                .build();
    }

    public Question toEntity(QuestionDto questionDto){
        if (questionDto == null){
            return null;
        }
        Question question = new Question();
            question.setId(questionDto.getId());
            question.setLibelle(questionDto.getLibelle());
            question.setResponses((List<Response>) questionDto.getReponses());
        return question;
    }
}
