package com.gilgamesh.gestiondestock.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gilgamesh.gestiondestock.models.*;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class QuestionaireDto {

    private Long id;

    private String code;

    private String titre;

    private String description;

    private String image;

    @JsonIgnore
    private Utilisateur utilisateur;

    @JsonIgnore
    private List<Question> questions;

    @JsonIgnore
    private List<Evaluation> evaluations;

    public QuestionaireDto fromEntity(Questionaire questionaire){
        if (questionaire == null){
            return null;
        }
        return QuestionaireDto.builder()
                .id(questionaire.getId())
                .code(questionaire.getCode())
                .titre(questionaire.getTitre())
                .description(questionaire.getDescription())
                .image(questionaire.getImage())
                .build();
    }

    public Questionaire toEntity(QuestionaireDto questionaireDto){
        if (questionaireDto == null){
            return null;
        }
        Questionaire questionaire = new Questionaire();
            questionaire.setId(questionaireDto.getId());
            questionaire.setCode(questionaireDto.getCode());
            questionaire.setTitre(questionaireDto.getTitre());
            questionaire.setDescription(questionaireDto.getDescription());
            questionaire.setImage(questionaireDto.getImage());
        return questionaire;
    }


}
