package com.gilgamesh.gestiondestock.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;
import com.gilgamesh.gestiondestock.models.Roles;
import com.gilgamesh.gestiondestock.models.Utilisateur;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UtilisateurDto {

    private Long id;

    private String nom;

    private String prenom;

    private String email;

    private String num;

    private Character sexe;

    private String photo;

    @JsonIgnore
    List<Evaluation> evaluationList;

    @JsonIgnore
    List<Questionaire> questionaires;

    @JsonIgnore
    List<Roles> roles;

    public UtilisateurDto fromEntity(Utilisateur utilisateur){
        if (utilisateur == null){
            return null;
        }
        return UtilisateurDto.builder()
                .id(utilisateur.getId())
                .nom(utilisateur.getNom())
                .prenom(utilisateur.getPrenom())
                .email(utilisateur.getEmail())
                .num(utilisateur.getNum())
                .sexe(utilisateur.getSexe())
                .photo(utilisateur.getPhoto())
                .build();
    }

    public Utilisateur toEntity(UtilisateurDto utilisateurDto){
        if (utilisateurDto == null){
            return null;
        }
        Utilisateur utilisateur = new Utilisateur();

        utilisateur.setId(utilisateurDto.getId());
        utilisateur.setNom(utilisateurDto.getNom());
        utilisateur.setPrenom(utilisateurDto.getPrenom());
        utilisateur.setEmail(utilisateurDto.getEmail());
        utilisateur.setNum(utilisateurDto.getNum());
        utilisateur.setSexe(utilisateurDto.getSexe());
        utilisateur.setPhoto(utilisateurDto.getPhoto());

        return utilisateur;
    }
}
