package com.gilgamesh.gestiondestock.repository;

import com.gilgamesh.gestiondestock.models.Evaluation;
import com.gilgamesh.gestiondestock.models.Questionaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvaluationRepository extends JpaRepository<Evaluation, Long> {

    List<Evaluation> findEvaluationByQuestionaire(Questionaire questionaire);
}
