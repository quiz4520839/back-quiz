package com.gilgamesh.gestiondestock.repository;

import com.gilgamesh.gestiondestock.models.Questionaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionaireRepository extends PagingAndSortingRepository<Questionaire, Long> {
}
