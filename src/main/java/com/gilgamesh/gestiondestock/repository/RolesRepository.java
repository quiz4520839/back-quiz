package com.gilgamesh.gestiondestock.repository;

import com.gilgamesh.gestiondestock.models.ERole;
import com.gilgamesh.gestiondestock.models.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Long> {
    Optional<Roles> findByName(ERole name);
}
