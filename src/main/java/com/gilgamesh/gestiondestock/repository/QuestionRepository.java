package com.gilgamesh.gestiondestock.repository;

import com.gilgamesh.gestiondestock.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}
