package com.gilgamesh.gestiondestock.validators;

import com.gilgamesh.gestiondestock.dto.UtilisateurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class UtilisateurValidator {

    public static List<String> validate(UtilisateurDto utilisateurDto){
        List<String> errors = new ArrayList<>();

        if (utilisateurDto == null){
            errors.add("Veuillez renseigner vos information personnelle avant la validation");
        }

        if (!StringUtils.hasLength(utilisateurDto.getNom())){
            errors.add("Veuillez renseigner votre nom");
        }
        if (!StringUtils.hasLength(utilisateurDto.getPrenom())){
            errors.add("Veuillez renseigner votre premom");
        }
        if (!StringUtils.hasLength(utilisateurDto.getEmail())){
            errors.add("Veuillez renseigner votre Email");
        }
        if (!StringUtils.hasLength(utilisateurDto.getNum())){
            errors.add("Veuillez renseigner votre numero de telephone");
        }

        return errors;
    }
}
