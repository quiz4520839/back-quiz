package com.gilgamesh.gestiondestock.validators;

import com.gilgamesh.gestiondestock.dto.EvaluationDto;
import com.gilgamesh.gestiondestock.dto.QuestionaireDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class EvaluationValidator {
    public static List<String> validate(EvaluationDto evaluationDto){
        List<String> errors = new ArrayList<>();

        if (evaluationDto == null){
            errors.add("Veuillez renseigner les information sur le evaluation en cours");
        }

        if (evaluationDto.getDateEval() == null){
            errors.add("Veuillez renseigner une date d'evaluation");
        }
        if (evaluationDto.getQuestionaire() == null){
            errors.add("Veuillez renseigner le quiz de evaluation");
        }

        if (evaluationDto.getUtilisateur() == null){
            errors.add("Veuillez renseigner l'utilisateur qui est evaluer");
        }

        return errors;
    }
}
