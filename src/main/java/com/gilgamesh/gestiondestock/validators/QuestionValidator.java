package com.gilgamesh.gestiondestock.validators;

import com.gilgamesh.gestiondestock.dto.QuestionDto;
import com.gilgamesh.gestiondestock.dto.UtilisateurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class QuestionValidator {
    public static List<String> validate(QuestionDto questionDto){
        List<String> errors = new ArrayList<>();

        if (questionDto == null){
            errors.add("Veuillez renseigner la information sur la question");
        }

        if (!StringUtils.hasLength(questionDto.getLibelle())){
            errors.add("Veuillez renseigner le libelle de la question");
        }
        if (questionDto.getReponses() == null){
            errors.add("Veuillez renseigner les reponses de cette question");
        }
        if (questionDto.getQuestionaire() == null){
            errors.add("Veuillez renseigner le questionaire de la question a enregistrer");
        }

        return errors;
    }
}
