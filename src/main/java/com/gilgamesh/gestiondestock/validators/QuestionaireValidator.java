package com.gilgamesh.gestiondestock.validators;

import com.gilgamesh.gestiondestock.dto.QuestionDto;
import com.gilgamesh.gestiondestock.dto.QuestionaireDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class QuestionaireValidator {
    public static List<String> validate(QuestionaireDto questionaireDto){
        List<String> errors = new ArrayList<>();

        if (questionaireDto == null){
            errors.add("Veuillez renseigner la information sur le quiz");
        }

        if (!StringUtils.hasLength(questionaireDto.getCode())){
            errors.add("Veuillez renseigner le code du quiz");
        }
        if (!StringUtils.hasLength(questionaireDto.getTitre())){
            errors.add("Veuillez renseigner le titre du quiz");
        }

        if (questionaireDto.getUtilisateur() == null){
            errors.add("Veuillez renseigner l'utilisateur qui cree le quiz'");
        }

        return errors;
    }
}
