package com.gilgamesh.gestiondestock.utils;

public interface Constants {

  String APP_ROOT = "/api/v1";
  String AUTHS = APP_ROOT + "/auths";
  String QUIZ = APP_ROOT + "/quiz";
  String QUESTION = APP_ROOT + "/question";
  String PARTICIPATION = APP_ROOT + "/participation";

  String LOGIN = AUTHS + "/login";
  String REGISTER = AUTHS + "/register";
  String GET_ALL_USERS = AUTHS + "/all";
  String GET_USER = AUTHS + "/one/{id}";
  String DELETE_USER = AUTHS + "/delete/{id}";

  String CREATE_QUIZ = QUIZ + "/create";
  String ALL_QUIZ = QUIZ + "/all";
  String ONE_QUIZ = QUIZ + "/one/{id}";
  String DELETE_QUIZ = QUIZ + "/delete/{id}";

  String CREATE_QUESTION = QUESTION + "/create";
  String ALL_QUESTION = QUESTION + "/all";
  String ONE_QUESTION = QUESTION + "/one/{id}";
  String DELETE_QUESTION = QUESTION + "/delete/{id}";

  String CREATE_PARTICIPATION = PARTICIPATION + "/create";
  String ALL_PARTICIPATION = PARTICIPATION + "/all";
  String ONE_PARTICIPATION = PARTICIPATION + "/one/{id}";
  String QUIZ_PARTICIPATION = PARTICIPATION + "/quiz";
  String DELETE_PARTICIPATION = PARTICIPATION + "/delete/{id}";


}
